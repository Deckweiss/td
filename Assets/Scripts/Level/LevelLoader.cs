﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public class LevelLoader : MonoBehaviour{

    [SerializeField]
    private GameObject tilePrefab;
    [SerializeField]
    private Wave Wave;
    
    private GameManager gameManager;
    
    
    // Use this for initialization
    void Start(){
        gameManager = GameManager.manager;
        LoadLevelFromFile("Level");   //For testing purposes only
                            //TODO add level.txt parameter to the function, so that it can be called
                            //from outside (game menu, level select).
                            // no need to inherit monoBehaviour and override Start after that.
    }
    
    //Load level, load path.
    public void LoadLevelFromFile(string fileName){
        float tileSize = tilePrefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        char[,] charMap = ReadLevelText(fileName);
        
        PathLoader pathLoader = this.gameObject.GetComponent<PathLoader>();
        GameObject path = pathLoader.LoadPath(charMap, tileSize);
        
        LevelManager levelManager = gameManager.getLevel().GetComponent<LevelManager>();
        levelManager.CreateLevel(charMap, tileSize);
        
        GameObject spawner = GameObject.Find("SpawnManager");
        spawner.GetComponent<SpawnManager>().Spawn(path.GetComponent<MovementPath>(),Wave);
        
        
        
        //pathLoader.LoadPath(charMap, tileSize);
        //path.transform.position += offset + new Vector3(0,-tileSize/3.1f, 0);
        //Destroy(gameObject); does it make sence to keep the gameoject for later reuse ?
    }
    
    //Load level from "Level.txt"
    private char[,] ReadLevelText(string fileName){
        TextAsset bindData = Resources.Load(fileName) as TextAsset;
        string[] data = bindData.text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        int h = data.Length;
        //int w = data[0].Length;
        char[][] outPut = new char[h][];
        
        for(int i = 0; i < h; i++){
            outPut[i] = data[i].ToCharArray();
        }
        
        return To2D(outPut);
    }
    
    //Methode that converts Jagged array to 2d arrays
    private static T[,] To2D<T>(T[][] source){
        try
        {
            int FirstDim = source.Length;
            int SecondDim = source.GroupBy(row => row.Length).Single().Key; 
        // throws InvalidOperationException if source is not rectangular

            var result = new T[FirstDim, SecondDim];
            for (int i = 0; i < FirstDim; ++i)
                for (int j = 0; j < SecondDim; ++j)
                    result[i, j] = source[i][j];
    
            return result;
        }
        catch (InvalidOperationException)
        {
            throw new InvalidOperationException("The given jagged array is not rectangular.");
        } 
    }
}
