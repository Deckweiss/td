﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TileManager : MonoBehaviour {

        [SerializeField]
        private TileDataList dataList;
        private Dictionary<char, TileData> tileSprites;
        
        void Awake() {
            tileSprites = new Dictionary<char, TileData>();
            LoadSprites();
        }
        
        void LoadSprites(){
            foreach (TileData td in dataList.Tiles){
                tileSprites.Add(td.Encoding, td);
            }
        }
        
        public Sprite getSprite(char c){
            TileData output;
            if (!tileSprites.TryGetValue(c, out output)) {
                // the key isn't in the dictionary.
                return null;
            }
            // output is now equal to the value
            return output.TileSprite;
        }

        public bool getBuildable(char c){
            TileData output;
            if (!tileSprites.TryGetValue(c, out output)) {
                // the key isn't in the dictionary.
                return false;
            }
            // output is now equal to the value
            return output.Buildable;
        }
}
