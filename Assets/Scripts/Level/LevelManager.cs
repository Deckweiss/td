﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

        [SerializeField]
        private GameObject tile;
        [SerializeField]
        private GameObject tower;
        [SerializeField]
        private GameObject outlineBox;
        [SerializeField]
        private Color buildableOutlineColor;
        [SerializeField]
        private Color unbuildableOutlineColor;
        [SerializeField]
        private Color invisibleOutlineColor;
        [SerializeField]
        private Color buildableShadowColor;
        [SerializeField]
        private Color unbuildableShadowColor;
        

        private char[,] charMap;
        private GameObject[,] tileMap;
        private GameObject[,] towerMap;
        private Vector3 offset;
        private float tileSize;
        private CartesianIsometricConverter converter;
        private GameObject level;
        private GameObject tiles;
        private TileManager tileManager;
        public Bounds bound;
        
        //Global variables for the two mouse-over and the placeTower functions
        private IntVector2 arrayPositionOfTile;
        private bool isBuildable;
        
        // Use this for initialization
        void Awake () {
            bound = new Bounds(Vector3.zero, Vector3.zero);
            outlineBox = Instantiate(outlineBox);
            
            
            level = this.gameObject;
            converter = this.gameObject.GetComponent<CartesianIsometricConverter>();
            tileManager = this.gameObject.GetComponent<TileManager>();
        }
        
        void Start(){
            GameManager.manager.setLevelManager(this);
        }
        
        // Update is called once per frame
        void Update () {
        }
        
        //Creates a level from a charMap. Char to tile translation is defined in the tileManager
        public void CreateLevel(char[,] charMap, float tileSize){
            this.charMap = charMap;
            this.tileSize = tileSize;
            this.tiles = GameManager.manager.getTileList();
            
            
            int mapWidth = charMap.GetLength(1);
            int mapHeight = charMap.GetLength(0);
            tileMap = new GameObject[mapHeight, mapWidth];
            towerMap = new GameObject[mapHeight, mapWidth];
            offset = CalculateOffsets(mapWidth, mapHeight);
            
            for (int y = 0; y < mapHeight; y++){
                for (int x = 0; x < mapWidth; x++){
                    Sprite spriteToPlace = tileManager.getSprite(charMap[y,x]);
                    if(spriteToPlace != null){
                        // Needed for render order, looks wrong if z is the same for all tiles
                        float zOffset = -(float)(x+y)/100;
            
                        Vector3 tileCartesianPos = new Vector3(tileSize * x, tileSize * y, zOffset);
                        Vector3 tileIsometricPos = converter.CartesianToIsometric(tileCartesianPos);
                        tileMap[y,x] = PlaceTile(tileIsometricPos, spriteToPlace);
                    }
                }
            }
            level.transform.position += offset;
            setBound(mapWidth, mapHeight);
        }
        
        // Returns an offset that can be used to translate the level such as it is centered on the screen
        private Vector3 CalculateOffsets(int width, int height){
            Vector3 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2));
            Vector3 centeringOffset = new Vector3(
                                        -(0.25f * (width - height)*tileSize),
                                        (0.125f * (width + height)*tileSize),
                                        10.0f);
            return screenCenter + centeringOffset;
        }
        
        //Places a given tile at position.
        private GameObject PlaceTile(Vector3 position, Sprite sprite){
            GameObject newTile = Instantiate(tile);
            newTile.GetComponent<SpriteRenderer>().sprite = sprite;
            newTile.transform.position = position;
            newTile.transform.parent = tiles.transform;
            return newTile;
            
        }
        
        //Creates a bound, that is used as restriciton for camera movement in the InputHandler
        private void setBound(int mapWidth, int mapHeight){
            bound.Encapsulate(tileMap[0, 0].transform.position);
            bound.Encapsulate(tileMap[mapHeight-1, 0].transform.position);
            bound.Encapsulate(tileMap[0, mapWidth-1].transform.position);
            bound.Encapsulate(tileMap[mapHeight-1, mapWidth-1].transform.position);
            bound.Expand(tileSize*1.5f);
            bound.Encapsulate(Camera.main.transform.position - 
                    new Vector3(Camera.main.pixelWidth/43 ,Camera.main.pixelHeight/43, 0)/2);
            bound.Encapsulate(Camera.main.transform.position + 
                    new Vector3(Camera.main.pixelWidth/43 ,Camera.main.pixelHeight/43, 0)/2);
            
        }
        
        private bool isTileFree(int x, int y){
            return towerMap[y, x] == null;
        }
        
        //Places down a tower inside the tile GO, if possible.
        private GameObject PlaceTower(IntVector2 arrayPosition, GameObject tower){
            GameObject parentTile = tileMap[arrayPosition.y, arrayPosition.x];
            
            tower.transform.position = new Vector3(0, 0, -0.007f);
            tower.transform.SetParent(parentTile.transform, false);
            tower.GetComponent<GenericTower>().enabled = true;  // um den Tower zu "starten"
            return tower;
        }
        
        public void placeTowerClick(RaycastHit2D hit, GameObject tower){
            //IntVector2 arrayPositionOfTile = getArrayPosFromHitCollider(hit);
            //bool isBuildable = tileManager.getBuildable(charMap[arrayPositionOfTile.y,
            //                                                            arrayPositionOfTile.x]);
                                                                        
            if(isBuildable && isTileFree(arrayPositionOfTile.x, arrayPositionOfTile.y)){
                towerMap[arrayPositionOfTile.y , arrayPositionOfTile.x] = PlaceTower(arrayPositionOfTile, tower);
            }else{
                Destroy(tower);
            }
        }
        
        //Draws an outline of the tile the mouse is hovering. 
        public void mouseOverOutline(RaycastHit2D hit, bool overUI){
            if(hit.collider == null || overUI){
                //Debug.Log("nothing hit at: " + ray.origin);
                outlineBox.GetComponent<LineRenderer>().startColor = invisibleOutlineColor;
                outlineBox.GetComponent<LineRenderer>().endColor = invisibleOutlineColor;
                outlineBox.transform.position = new Vector3(-100,-100, outlineBox.transform.position.z);
            }            
            else{
                //Debug.Log("SOMETHING hit at: " + ray.origin);
                arrayPositionOfTile = getArrayPosFromHitCollider(hit);
                isBuildable = tileManager.getBuildable(charMap[arrayPositionOfTile.y,
                                                                        arrayPositionOfTile.x]);
                if (isBuildable && isTileFree(arrayPositionOfTile.x, arrayPositionOfTile.y)){
                    outlineBox.GetComponent<LineRenderer>().startColor = buildableOutlineColor;
                    outlineBox.GetComponent<LineRenderer>().endColor = buildableOutlineColor;
                } else {
                    outlineBox.GetComponent<LineRenderer>().startColor = unbuildableOutlineColor;
                    outlineBox.GetComponent<LineRenderer>().endColor = unbuildableOutlineColor;
                }
                
                outlineBox.transform.position = new Vector3(
                hit.collider.gameObject.transform.position.x,
                hit.collider.gameObject.transform.position.y,
                outlineBox.transform.position.z);
            }
        }
        
        //Draws an outline of the tile the mouse is hovering
        //and the shadow of the currently dragged tower at mouse position.
        public void mouseOverOutline(RaycastHit2D hit, GameObject towerShadow, bool overUI){
            mouseOverOutline(hit, overUI);
            if(hit.collider == null || overUI){
                towerShadow.transform.position = new Vector3(-100, -100, -100);
            }            
            else{
                if (isBuildable && isTileFree(arrayPositionOfTile.x, arrayPositionOfTile.y)){
                    towerShadow.GetComponent<SpriteRenderer>().color = buildableShadowColor;
                } else {
                    towerShadow.GetComponent<SpriteRenderer>().color = unbuildableShadowColor;
                }
                towerShadow.transform.position = new Vector3(
                hit.collider.gameObject.transform.position.x,
                hit.collider.gameObject.transform.position.y,
                hit.collider.gameObject.transform.position.z - 0.007f);
            }
        }
        
        private IntVector2 getArrayPosFromHitCollider(RaycastHit2D hit){
            Vector3 screenPosition = hit.collider.gameObject.transform.position;
            return getArrayPosFromScreenPos(screenPosition);
        }
        
        private IntVector2 getArrayPosFromScreenPos(Vector3 screenPosition){
            Vector3 temp = converter.IsometricToCartesian(screenPosition-offset)/2;
            return new IntVector2((int)temp.x, (int)temp.y);
        }
}
