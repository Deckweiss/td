﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    private GameObject enemy;
    private bool spawning=false;
    private int enemiestospawn;
    private float waittime;
    private float timeleft;
    private GameObject Enemies;
    private Queue<GameObject> enemylist;


    // Use this for initialization
    public void Spawn (MovementPath pathscript, Wave wave) {
        Enemies = GameManager.manager.getEnemyList();
        waittime = 1 / wave.frequency;
        timeleft = waittime;
        enemiestospawn = wave.enemycount;

        enemylist = new Queue<GameObject>();

        for(int x=0; x < enemiestospawn; x++)
        {
            enemy = Instantiate(wave.enemytype);
            enemy.transform.SetParent(Enemies.transform);
            enemy.GetComponent<GenericEnemy>().setPath(pathscript);
            enemy.SetActive(false);
            enemylist.Enqueue(enemy);
        }


        spawning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawning)
        {
            if (enemiestospawn > 0)
            {
                if (timeleft <= 0)
                {
                    enemylist.Dequeue().SetActive(true);
                    timeleft = waittime;
                    enemiestospawn--;
                }
                else
                {
                    timeleft -= Time.deltaTime;
                }
            }
            else
            {
                spawning = false;
            }

        }
    }
}
