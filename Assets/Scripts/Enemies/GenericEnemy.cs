﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericEnemy : MonoBehaviour
{

    private int HealthPoints;


    // Use this for initialization
    void Start()
    {
        HealthPoints = 100;
        SetPosition(new Vector3(0, 0, -1));

    }

    // Update is called once per frame
    void Update()
    {
    
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void TakeDamage(int damage, int type)
    {
        //Debug.Log("Took " + damage + " Damage");
        HealthPoints -= damage;
        //Debug.Log("CurrentHP "+HealthPoints);
        //Debug.Log(Time.time);
        Debug.Log("HIT");


        if (HealthPoints <= 0)
        {
            //Debug.Log("Enemy Died");
            Die();
        }
            
    }

    public void setPath(MovementPath path)
    {
        FollowPath followpath = GetComponentInParent<FollowPath>();
        followpath.MyPath = path;
    }

    public void Die()
    {
        //transform.rotation = Quaternion.Euler(0, 0, -90);
        //Debug.Log("DIEIDIEDI");
        Destroy(gameObject);
        //Handle Killing of the Enemy
    }

    public int getLife()
    {
        return HealthPoints;
    }

    public float getDistanceTravelled()
    {
        FollowPath fp = gameObject.GetComponent<FollowPath>();
        return fp.distancetravelled;

    }
}
    
