﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetingScriptFactory : MonoBehaviour {

    public static Type GetTargetingScript(TargetingScriptsEnum type)
    {
        Type targetscript;

        switch (type)
        {
            case TargetingScriptsEnum.OnPoint:
                targetscript = typeof(OnPoint);
                break;
            case TargetingScriptsEnum.Prefire:
                targetscript = typeof(Prefire);
                break;
            default:
                targetscript = null;
                break;

        }

        return targetscript;
    }
}
