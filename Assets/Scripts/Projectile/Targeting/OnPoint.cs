﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPoint : GenericTargetingScript {


    
public override void target()
    {
        projectile.target = getTargetbyTargetingMode();
        projectile.impactlocation = projectile.target.position;
    }
}
