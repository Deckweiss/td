﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericTargetingScript : MonoBehaviour
{
    protected Projectile projectile;

    public void Awake()
    {
        this.projectile = GetComponentInParent<Projectile>();
    }


    public abstract void target();



    protected Transform getTargetbyTargetingMode()
    {
        // Get appropiate Target by the method that is selected with towervars TargetingMode
        System.Comparison<GameObject> sortMethod = GetSortMethod();

        List<GameObject> enemiesinrange = new List<GameObject>();
        // get enemies in range
        float viewradius = (float)projectile.tower.getValue(TowerVarsEnum.ViewRadius);
        Transform enemylist = GameManager.manager.getEnemyList().transform;
        foreach (Transform enemy in enemylist)
        {
            if (PointInsideSphere(enemy.position, projectile.tower.transform.parent.position, viewradius))
            {
                enemiesinrange.Add(enemy.gameObject);
            }
        }
        // "sort" enemies by the appropiate method, 
        enemiesinrange.Sort(sortMethod);


        // take the first one and return it

        return enemiesinrange[0].transform;
    }
    private bool PointInsideSphere(Vector2 point, Vector2 centerofcircle, float radius)
    {
        return Vector2.Distance(point, centerofcircle) < radius;
    }


    private System.Comparison<GameObject> GetSortMethod()
    {
        System.Comparison<GameObject> sortMethod;
        switch ((TargetingModesEnum)projectile.tower.getValue(TowerVarsEnum.TargetingMode))
        {
            case TargetingModesEnum.LowestLife:
                sortMethod=lowestlife;
                break;
            case TargetingModesEnum.HighestLife:
                sortMethod = highestlife;
                break;
            case TargetingModesEnum.NearestfromTower:
                sortMethod = nearesttotower;
                break;
            case TargetingModesEnum.FurthestfromTower:
                sortMethod = furthestfromtower;
                break;
            case TargetingModesEnum.FirstinPath:
                sortMethod = firstinpath;
                break;
            case TargetingModesEnum.LastinPath:
                sortMethod = lastinpath;
                break;
            default:
                sortMethod = null;
                break;


        }

        return sortMethod;
    }


    private static int lowestlife(GameObject first, GameObject second)
    {
        int hpfirst = first.GetComponent<GenericEnemy>().getLife();
        int hpsecond = second.GetComponent<GenericEnemy>().getLife();

        if (hpfirst < hpsecond)
            return -1;
        if (hpfirst > hpsecond)
            return 1;

        return 0;
    }
    private  int highestlife(GameObject first, GameObject second)
    {
        return -1 * lowestlife(first,second); 
    }

    private  int nearesttotower(GameObject first, GameObject second)
    {
        float distancefirst = Vector2.Distance(first.transform.position, projectile.tower.transform.position);
        float distancesecond = Vector2.Distance(second.transform.position, projectile.tower.transform.position);

        if (distancefirst < distancesecond)
            return -1;
        if (distancefirst > distancesecond)
            return 1;

        return 0;
    }

    private int furthestfromtower(GameObject first, GameObject second)
    {
        return -1 * nearesttotower(first, second);
    }

    private int lastinpath(GameObject first, GameObject second)
    {
        float distancefirst = first.GetComponent<GenericEnemy>().getDistanceTravelled();
        float distancesecond = second.GetComponent<GenericEnemy>().getDistanceTravelled();

        if (distancefirst < distancesecond)
            return -1;
        if (distancefirst > distancesecond)
            return 1;

        return 0;
    }

    private int firstinpath(GameObject first, GameObject second)
    {
        return -1 * lastinpath(first, second);
    }







}