﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetingModesEnum {

    FirstinPath,
    LastinPath,
    LowestLife,
    HighestLife,
    NearestfromTower,
    FurthestfromTower
}
