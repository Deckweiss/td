﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileState
{
    Targeting,
    Pathing,
    Damaging,
    Finished
}