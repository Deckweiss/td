﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public Transform target;
    public ProjectileState projectileState;
    public GenericTower tower;
    public Vector3 impactlocation;

    private void Start()
    {
        projectileState = ProjectileState.Targeting;
    }

    public void setTowerScript(GenericTower tower)
    {
        this.tower = tower;
    }


    private void OnDrawGizmos()
    {
        if (target != null && false)
        {

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(target.position, (float)0.5);
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(impactlocation, (float)0.5);
            Gizmos.color = Color.gray;
            Gizmos.DrawSphere(transform.position, (float)0.5);
        }
    }
}
