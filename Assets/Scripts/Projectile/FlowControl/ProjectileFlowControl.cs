﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFlowControl : MonoBehaviour {
    GenericDamageScript damageScript;
    GenericProjectilePathingScript pathingScript;
    GenericTargetingScript targetingScript;
    private Projectile projectile;


	// Use this for initialization
	private void Start () {

        this.projectile = GetComponentInParent<Projectile>();

        Type targetingScripttype = TargetingScriptFactory.GetTargetingScript((TargetingScriptsEnum) projectile.tower.getValue(TowerVarsEnum.TargetingScriptType));
         if (targetingScripttype != null)
         {
             targetingScript = (GenericTargetingScript)gameObject.AddComponent(targetingScripttype);
         }

        Type pathingscripttype = PathingScriptFactory.GetProjectilePathingScript((ProjectilePathingScriptsEnum)projectile.tower.getValue(TowerVarsEnum.PathingScriptType));
        if (pathingscripttype != null)
        {
            pathingScript = (GenericProjectilePathingScript) gameObject.AddComponent(pathingscripttype);
        }

        Type damagescripttype = DamageScriptFactory.GetDamageScript((DamageScriptsEnum)projectile.tower.getValue(TowerVarsEnum.DamageScriptType));
        if (damagescripttype != null)
        {
            damageScript = (GenericDamageScript) gameObject.AddComponent(damagescripttype);
        }
        

	}

    private void Update()
    {
        if(projectile.target==null && projectile.impactlocation == null)
        {
            Destroy(gameObject);
        }
        switch (projectile.projectileState)
        {
            case ProjectileState.Targeting:
                targetingScript.target();
                projectile.projectileState = ProjectileState.Pathing;
                break;
            case ProjectileState.Pathing:
                if (pathingScript.pathe())
                {
                    projectile.projectileState = ProjectileState.Damaging;
                }                    
                break;
            case ProjectileState.Damaging:
                Debug.Log("DAMAGING");
                damageScript.DealDamage();
                projectile.projectileState = ProjectileState.Finished;
                break;
            case ProjectileState.Finished:
                //if chaining, then reset projectile state
                //else destroy
                Debug.Log("Finished");
                DestroyImmediate(gameObject);
                break;

        }
    }

}
