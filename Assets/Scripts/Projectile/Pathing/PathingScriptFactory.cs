﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathingScriptFactory  {

    public static Type GetProjectilePathingScript(ProjectilePathingScriptsEnum type)
    {
        Type movescript;

        switch (type)
        {
            case ProjectilePathingScriptsEnum.Homing:
                movescript = typeof(Homing);
                break;
            case ProjectilePathingScriptsEnum.DirectHit:
                movescript = typeof(DirectHit);
                break;
            case ProjectilePathingScriptsEnum.ToPoint:
                movescript = typeof(ToPoint);
                break;
            default:
                movescript = null;
                break;

        }

        return movescript;
    }

}
