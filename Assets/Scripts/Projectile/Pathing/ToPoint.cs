﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToPoint : GenericProjectilePathingScript {
    public override bool pathe()
    {
        if (projectile.target != null)
        {



            transform.position = Vector3.MoveTowards(transform.position,
                                                                    projectile.impactlocation,
                                                                    Time.deltaTime * (float)projectile.tower.getValue(TowerVarsEnum.ProjectileSpeed));

            if (Vector3.Distance(transform.position, projectile.impactlocation) <= 0)
            {
                Debug.Log("Got to Location");
                return true;
            }

            
        }
        else
        {
            Destroy(gameObject);
        }
        return false;
    }

}
