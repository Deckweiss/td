﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : GenericProjectilePathingScript {
    Boolean ispathing;
	// Use this for initialization

    private void Start()
    {
        if ((float)projectile.tower.getValue(TowerVarsEnum.AutoDestroyAfterSecs) > 0)
        {
            StartCoroutine(AutoExplode());
        }
    }

    private IEnumerator AutoExplode()
    {
        yield return new WaitForSeconds((float)projectile.tower.getValue(TowerVarsEnum.AutoDestroyAfterSecs));
        ExplodeSelf();
    }

    private void ExplodeSelf()
    {
        Destroy(gameObject);
    }

    public override bool pathe()
    {
        if (projectile.target != null)
        {


            float distancefromtargettohit = 0.05f;



            transform.position = transform.position = Vector3.MoveTowards(transform.position,
                                                                    projectile.target.position,
                                                                    Time.deltaTime * (float)projectile.tower.getValue(TowerVarsEnum.ProjectileSpeed));

            if (projectile.target != null)
            {
                Vector3 relativePos = projectile.target.position - transform.position;
                if (relativePos != Vector3.zero)
                {
                    Quaternion rotation = Quaternion.LookRotation(relativePos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, (float)projectile.tower.getValue(TowerVarsEnum.HomingSensitivity));
                }
            }
            transform.Translate(0, 0, (float)projectile.tower.getValue(TowerVarsEnum.ProjectileSpeed) * Time.deltaTime, Space.Self);




            if (Vector3.Distance(transform.position, projectile.target.position) < distancefromtargettohit)
            {
                Debug.Log("Homing is here");
                return true; // pathing finished
            }

            
        }
        return false;
    }
}
