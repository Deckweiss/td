﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericProjectilePathingScript : MonoBehaviour {
    protected Projectile projectile;

    public void Awake()
    {
        this.projectile = GetComponentInParent<Projectile>(); 
    }

    public abstract bool pathe();
}
