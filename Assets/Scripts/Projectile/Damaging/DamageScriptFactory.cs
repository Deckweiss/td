﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScriptFactory : MonoBehaviour {

    public static Type GetDamageScript(DamageScriptsEnum type)
    {
        Type damagescript;

        switch (type)
        {
            case DamageScriptsEnum.CircleDamage:
                damagescript = typeof(CircleDamage);
                break;
            case DamageScriptsEnum.LineDamage:
                damagescript = typeof(LineDamage);
                break;
            case DamageScriptsEnum.SingleTarget:
                damagescript = typeof(SingleTarget);
                break;
            default:
                damagescript = null;
                break;

        }

        return damagescript;
    }
}