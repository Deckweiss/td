﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTarget : GenericDamageScript {
    public override void DealDamage()
    {
        if (projectile.target != null)
        {
            Transform enemytransform = projectile.target;
            enemytransform.gameObject.GetComponentInParent<GenericEnemy>().TakeDamage(damage, damagetype);
        }
    }
}
