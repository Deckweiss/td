﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDamage : GenericDamageScript {
    private float length;
    private float width;
    private Vector2 direction;

    public override void DealDamage()
    {
        this.length = (float)projectile.tower.getValue(TowerVarsEnum.HitboxLength);
        this.width = (float)projectile.tower.getValue(TowerVarsEnum.HitboxWidth);
        this.direction = (Vector2)projectile.tower.getValue(TowerVarsEnum.HitboxDirection);
        Transform enemylist = GameManager.manager.getEnemyList().transform;

        Vector2 baselinedirection = RotatePerpendicularCounterClockwise(direction).normalized;
        Vector2 rectbasepoint = ((Vector2)projectile.tower.transform.position) + baselinedirection * width / 2;

        Rect hitbox = new Rect(rectbasepoint.x, rectbasepoint.y, width, length);


        foreach (Transform enemy in enemylist)
        {
            if (hitbox.Contains(enemy.transform.position)){
                enemy.gameObject.GetComponent<GenericEnemy>().TakeDamage(damage, damagetype);
            }

        }


    }

    private Vector2 RotatePerpendicularCounterClockwise(Vector2 vector2)
    {
        return new Vector2(vector2.y, -vector2.x);
    }
}
