﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDamage : GenericDamageScript {

    public override void DealDamage()
    {
        float areaofeffectradius = (float)projectile.tower.getValue(TowerVarsEnum.AreaofEffect);
        Transform enemylist = GameManager.manager.getEnemyList().transform;
        foreach (Transform enemy in enemylist)
        {
            if (PointInsideSphere(enemy.transform.position, transform.position, areaofeffectradius))
            {
                enemy.gameObject.GetComponent<GenericEnemy>().TakeDamage(damage,damagetype);
            }
        }

    }

    public void Start()
    {


    }

    private bool PointInsideSphere( Vector2 point ,Vector2 centerofcircle, float radius)
    {
        return Vector2.Distance(point, centerofcircle) < radius;
    }
}
