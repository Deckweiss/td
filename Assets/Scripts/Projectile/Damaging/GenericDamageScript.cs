﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericDamageScript : MonoBehaviour{
     protected int damage;
     protected int damagetype;
     protected Projectile projectile;


    public void Awake()
    {
        this.projectile = GetComponentInParent<Projectile>();
        this.damage = (int) projectile.tower.getValue(TowerVarsEnum.Damage);
        this.damagetype = (int)projectile.tower.getValue(TowerVarsEnum.DamageType);
    }

    public abstract void DealDamage();

 
}
