﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Catalogue of sprites by char label, declared via scriptableObjects in data.
public class SpriteManager : MonoBehaviour {

        [SerializeField]
        private TileDataList dataList;
        private Dictionary<char, Sprite> tileSprites;
        
        void Awake() {
            tileSprites = new Dictionary<char, Sprite>();
            LoadSprites();
        }
        
        void LoadSprites(){
            foreach (TileData td in dataList.Tiles){
                tileSprites.Add(td.Encoding, td.TileSprite);
            }
        }
        
        public Sprite getSprite(char c){
            Sprite output;
            if (!tileSprites.TryGetValue(c, out output)) {
                // the key isn't in the dictionary.
                return null;
            }
            // output is now equal to the value
            return output;
        }

        
        
}
