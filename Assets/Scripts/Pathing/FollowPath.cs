﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPath : MonoBehaviour
{
    #region Enums
    public enum MovementType  //Type of Movement
    {
        MoveTowards,
        LerpTowards
    }
    #endregion //Enums

    #region Public Variables
    public MovementType Type = MovementType.MoveTowards; // Movement type used
    public MovementPath MyPath; // Reference to Movement Path Used
    public float Speed = 1; // Speed object is moving
    public float MaxDistanceToGoal = .1f; // How close does it have to be to the point to be considered at point
    #endregion //Public Variables

    #region Private Variables
    private Transform pointInPath; //Used to reference points returned from MyPath.GetNextPathPoint
    public int pointindex = 0;
    public float distancetravelled = 0;
    #endregion //Private Variables

    // (Unity Named Methods)
    #region Main Methods
    public void Start()
    {
        //Make sure there is a path assigned
        if (MyPath == null)
        {
            Debug.LogError("Movement Path cannot be null, I must have a path to follow.", gameObject);
            return;
        }

        //Sets up a reference to an instance of the coroutine GetNextPathPoint
        pointInPath = MyPath.getPoint(pointindex);
        //Debug.Log(pointInPath);
        //Get the next point in the path to move to (Gets the Default 1st value)
        pointindex++;
        //Debug.Log(pointInPath);

        //print(pointInPath.tag);

        //Make sure there is a point to move to
        if (pointInPath == null)
        {
            Debug.LogError("A path must have points in it to follow", gameObject);
            return; //Exit Start() if there is no point to move to
        }

        //Set the position of this object to the position of our starting point
        transform.position = pointInPath.position;
        pointInPath = MyPath.getPoint(pointindex);

        //Debug.Log("EndofStart");
    }

    //Update is called by Unity every frame
    public void Update()
    {

        //Validate there is a path with a point in it
        if (pointInPath == null)
        {
            return; //Exit if no path is found
        }


        //Move to the next point in path using MoveTowards
        /* --DEPRECATED OLD MOVEMENT--
         * transform.position =
            Vector3.MoveTowards(transform.position,
                                pointInPath.position,
                                Time.deltaTime * Speed);
        */

        float distanceToCover = Speed * Time.deltaTime;
        float distanceToNextPoint;
        while (distanceToCover > 0)
        {
            distanceToNextPoint = Vector3.Distance(transform.position, pointInPath.position);
            if (distanceToCover < distanceToNextPoint)
            {
                if (Type == MovementType.MoveTowards) //If you are using MoveTowards movement type
                {
                    //Move towards the point in path using Move
                    transform.position = Vector3.MoveTowards(transform.position, 
                                                                pointInPath.position,
                                                                distanceToCover);
                }
                else
                if (Type == MovementType.LerpTowards) //If you are using LerpTowards movement type
                {
                    //Move towards the next point in path using Lerp
                    transform.position = Vector3.Lerp(transform.position,
                                                        pointInPath.position,
                                                        distanceToCover);
                }
                distancetravelled += distanceToCover;
                distanceToCover = 0;
            }
            else
            {
                transform.position = pointInPath.position;
                if (pointindex == MyPath.PathSequence.Length - 1)
                {
                    //despawn entity
                    GetComponentInParent<GenericEnemy>().Die();
                    distanceToCover = 0; // is needed to stop the Loop
                }
                else
                {
                    pointInPath = MyPath.getPoint(++pointindex);
                }
                distancetravelled += distanceToNextPoint;
                distanceToCover -= distanceToNextPoint;

            }
        }
        


    }


    //Check to see if you are close enough to the next point to start moving to the following one
    //Using Pythagorean Theorem
    //per unity suaring a number is faster than the square root of a number
    //Using .sqrMagnitude 
    /*var distanceSquared = (transform.position - pointInPath.position).sqrMagnitude;
    if (distanceSquared < MaxDistanceToGoal * MaxDistanceToGoal) //If you are close enough
    {
        if (pointindex == MyPath.PathSequence.Length - 1)
        {
            //despawn entity
            GetComponentInParent<GenericEnemy>().Die();
        }
        else
        {
            pointInPath = MyPath.getPoint(++pointindex);

        }

    }*/
    //The version below uses Vector3.Distance same as Vector3.Magnitude which includes (square root)
    /*
    var distanceSquared = Vector3.Distance(transform.position, pointInPath.Current.position);
    if (distanceSquared < MaxDistanceToGoal) //If you are close enough
    {
        pointInPath.MoveNext(); //Get next point in MovementPath
    }
    */

    #endregion //Main Methods

    //(Custom Named Methods)
    #region Utility Methods 

    #endregion //Utility Methods

    //Coroutines run parallel to other fucntions
    #region Coroutines

    #endregion //Coroutines
}
