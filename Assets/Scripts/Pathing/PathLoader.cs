﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathLoader : MonoBehaviour {

    [SerializeField]
    private GameObject[] pathPrefabs;
    private GameObject path;
    
    /*  (I can't really tell how this actually works myself after coding it for 5 hours)
    /   Calculates a path for the level and creates it as a gameobject called "EnemyPath"   
    /   with an attatched MovementPath script.
    /   pathPrefabs[0] is the Path prefab
    /   pathPrefabs[1] is the Point prefab
    */
    public GameObject LoadPath(char[,] levelMap, float tileSize){
        CartesianIsometricConverter converter = this.gameObject.GetComponent<CartesianIsometricConverter>();
        
        this.path = GameManager.manager.getEnemyPath();
        List<IntVector2> pathPoints = new List<IntVector2>();
        
        IntVector2 start = CoordinatesOfChar(levelMap, 'S');
        IntVector2 finish = CoordinatesOfChar(levelMap, 'F');
        if (start == new IntVector2(-1, -1)){
            throw new System.ArgumentException(" Start point does not exist on the map");
        }
        if (finish == new IntVector2(-1, -1)){
            throw new System.ArgumentException(" Finish point does not exist on the map");
        }
        
        
        IntVector2 currentTile = start;
        IntVector2 direction = new IntVector2(0, 0);
        pathPoints.Add(currentTile);
        
        int n = 0;
        while(currentTile != finish){
            IntVector2[] dirToCheck;
            if (n==0){
                dirToCheck = new IntVector2[]{IntVector2.up, IntVector2.down,   
                                              IntVector2.left, IntVector2.right};
            }else{
                IntVector2 l = direction;
                IntVector2 r = direction;
                r.RotateCW();
                l.RotateCCW();
                dirToCheck = new IntVector2[]{r, l};
            }
            direction = getDirection(currentTile, dirToCheck, levelMap);
            if (direction == new IntVector2(0, 0)){
                throw new System.ArgumentException("No valid way found");
            }
        
            while(isCharAt('0', currentTile + direction, levelMap)
                || isCharAt('F', currentTile + direction, levelMap)){
                currentTile += direction;
                if(currentTile == finish){
                    break;
                }
            }
            pathPoints.Add(currentTile);
            
            n++;
        } 
        
        MovementPath pathScript = path.GetComponent(typeof(MovementPath)) as MovementPath;
        pathScript.PathSequence = new Transform[pathPoints.Count];
        
        
        for(int i = 0; i< pathPoints.Count; i++){
        
            IntVector2 pos = pathPoints.ElementAt(i);
            float posZ = -((float)(pos.x+ pos.y)/100) -0.007f;
            GameObject tempPoint = Instantiate(pathPrefabs[1]);
            tempPoint.transform.parent = path.transform;
            tempPoint.transform.position = converter.CartesianToIsometric(
                new Vector3(pos.x * tileSize,
                            pos.y * tileSize, 
                            posZ));  //replace -1 with tile z
            tempPoint.name = "P"+i;
            pathScript.PathSequence[i] = tempPoint.transform;
        }
        path.transform.position += new Vector3(0,-tileSize/3.1f, 0);
        return path;
    }
    
    private IntVector2 getDirection(IntVector2 pos, IntVector2[] dirToCheck, char[,] levelMap){
        foreach (IntVector2 dir in dirToCheck){
            IntVector2 tmpT = pos + dir;
            if(tmpT.x <0 || tmpT.y <0 || tmpT.x > levelMap.GetLength(1) || tmpT.y > levelMap.GetLength(0)){
                break;
            }
            else if(isCharAt('0', tmpT, levelMap)){
                return dir;
            }
        }
        return new IntVector2(0, 0);
    }
    
    //Returns true if asked char is at said position in said char[,]
    private bool isCharAt(char c, IntVector2 pos, char[,] levelMap){
        return levelMap[pos.y, pos.x] == c;
    }
    
    // Finds value in a matrix and returns its coordinates
    private IntVector2 CoordinatesOfChar(char[,] matrix, char value)
    {
    int w = matrix.GetLength(0); // width
    int h = matrix.GetLength(1); // height

    for (int x = 0; x < w; ++x)
    {
        for (int y = 0; y < h; ++y)
        {
            if (matrix[x, y] == value)
                return new IntVector2(y, x);
        }
    }

    return new IntVector2(-1, -1);
    }
}
