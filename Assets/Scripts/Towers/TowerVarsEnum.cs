﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TowerVarsEnum
{
    ProjectileSprite,                 
    ViewRadius,
    AttackSpeed,            //attacks per second
    TargetingScriptType,
    TargetingMode,          //target first in path ,target last in path, target lowest life, target highest life, target nearest from tower, target furthest from tower
    StayonTarget,           
    PathingScriptType,
    DamageScriptType,
    Damage,
    DamageType,
    ProjectileSpeed,        // for flying projectiles (homing or not)
    HomingSensitivity,      // for homing projectiles     
                            /* Represents the homing sensitivity of the missile.
                               Range [0.0,1.0] where 0 will disable homing and 1 will make it follow the target like crazy.
                               This param is fed into the Slerp (defines the interpolation point to pick) */
    AutoDestroyAfterSecs,   // for homing projectiles/maybe traps/ground effects
    AreaofEffect,           // for aoe projectiles/effects 
    HitboxLength,           // for LineDamage
    HitboxWidth,            // for LineDamage
    HitboxDirection,        // for LineDamage
    Chainings,              // for ChainingProjectiles
    ChainAoe                // AoE in which to look for the next chain
}