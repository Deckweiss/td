﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericTower : MonoBehaviour {

    private Dictionary<TowerVarsEnum, object> values;
    private float nexttimetoshoot;
    [SerializeField]
    private GameObject projectileprefab;
    void Awake()
    {
        values = new Dictionary<TowerVarsEnum, object>();
    }

	// Use this for initialization
	void Start () {
        nexttimetoshoot = 1 / (float)values[TowerVarsEnum.AttackSpeed];
    }
	
	// Update is called once per frame
	void Update () {
        nexttimetoshoot -= Time.deltaTime;
        if (nexttimetoshoot <= 0)
        {
            shoot();
            nexttimetoshoot+= 1 / (float)values[TowerVarsEnum.AttackSpeed];
        }
		
	}

    private void shoot()
    {
        GameObject projectile = new GameObject("Projectile");
        projectile.transform.position = transform.position + new Vector3(0,0,-1);
        projectile.transform.SetParent(transform);
        Projectile proj = projectile.AddComponent<Projectile>();
        SpriteRenderer spriteRenderer = projectile.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = (Sprite) values[TowerVarsEnum.ProjectileSprite];
        proj.setTowerScript(this);
        projectile.AddComponent<ProjectileFlowControl>(); //this actually starts the projectile doing its stuff (i think)

    }

    public object getValue(TowerVarsEnum key)
    {
        object value;
        values.TryGetValue(key,out value);
        return value;
    }

    public void setValue(TowerVarsEnum key, object value)
    {
        values.Add(key, value);
    }

    public Dictionary<TowerVarsEnum,object> getValues()
    {
        return values;
    }

    public void addValues(Dictionary<TowerVarsEnum, object> newvalues)
    {
        foreach(KeyValuePair<TowerVarsEnum, object> entry in newvalues)
        {
            values.Add(entry.Key, entry.Value);
        }
    }

    public void setValues(Dictionary<TowerVarsEnum, object> newvalues)
    {
        values = newvalues;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, (float)getValue(TowerVarsEnum.ViewRadius));
    }
}
