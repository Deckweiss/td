﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartesianIsometricConverter : MonoBehaviour {

    private float unitIsoWidth = Mathf.Sqrt(2.5f);

    /*
    /   Transforms cartesian to isometric coordinates.
    /       xy                          00
    /   Eg: 00 10 20 30               01  10                                    +y
    /       01 11 21 31    --->     02  11  20                                   ^
    /       02 12 22 32               12  21  30                                 |
    /                                   22  31                                   |
    /                                     32                                    °+----> +x
    / The y coordinate is negative, because Unity uses the bottom left as origin °
    / and I use the top left in my Arrays as origin.
    */ 
    public Vector3 CartesianToIsometric(Vector3 cartesian){
        return new Vector3(
            (cartesian.x - cartesian.y)/2,
            -(cartesian.x + cartesian.y)/4,
            cartesian.z
        );
    }
    
    //Reverse function of the above
    public Vector3 IsometricToCartesian(Vector3 isometric){
        Vector3 cartPos = new Vector3(0, 0, isometric.z);
        cartPos.y = (isometric.x*2 + isometric.y*4)/-2 ;
        cartPos.x = (isometric.x*2 + cartPos.y);
        return cartPos;
    }
    
    //Checks if a point is in a radius, that is measured in tiles.
    //Radius in our isometric view is an ellipse which is twice as wide as high.
    //Width and height are calculated from tile size in screenSpace.
    public bool PointInsideIsometricRadius(Vector2 point, Vector2 origin, float radius){
        float width = radius * unitIsoWidth;
        float height = width/2;
        float x = point.x - origin.x;
        float y = point.y - origin.y;
        return ((x * x) / (width*width)) + ((y * y) / (height*height)) <= 1.0f;
    }
}
