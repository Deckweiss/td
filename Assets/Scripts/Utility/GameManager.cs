﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager manager;
    public static CartesianIsometricConverter converter;
    
    [SerializeField]
    private GameObject levelPrefab;
    [SerializeField]
    private GameObject pathPrefab;
    
    [SerializeField]
    private int PlayerHP;
    
    public LevelManager levelManager;
    private InputHandler inputHandler;
    
    private GameObject level;
    private GameObject enemyPath;
    private GameObject tileList;
    private GameObject enemyList;
    

    void Awake () {
        manager = this;
        converter = this.gameObject.GetComponent<CartesianIsometricConverter>();
        inputHandler = this.gameObject.GetComponent<InputHandler>();
        
        level = Instantiate(levelPrefab);
        level.name = "Level";
        enemyPath = Instantiate(pathPrefab);
        enemyPath.name = "EnemyPath";
        enemyPath.transform.SetParent(level.transform);
        tileList = new GameObject("TileList");
        tileList.transform.SetParent(level.transform);
        enemyList = new GameObject("EnemyList");
        enemyList.transform.SetParent(level.transform);
            
        //mainCamera = Camera.main;
    }
    
    void Update (){
        if(levelManager != null){
            inputHandler.controlledUpdate();
            hpCheck();
        }
    }
    
    public void setLevelManager(LevelManager lm){
        this.levelManager = lm;
        inputHandler.initializeFields();
    }
    
    private void hpCheck(){
        if(PlayerHP <= 0){
            print("Game Over"); //TODO add some game ending functionality
        }
    }
    
    public void takeDamage(int damage){
        PlayerHP -= damage;
    }
    
    //Getters
    public GameObject getLevel(){
        return level;
    }
    
    public GameObject getEnemyPath(){
        return enemyPath;
    }
    
    public GameObject getTileList(){
        return tileList;
    }
    
    public GameObject getEnemyList(){
        return enemyList;
    }
}
