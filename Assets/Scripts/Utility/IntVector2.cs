﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A Integer based version of Vector2, does not implement all of Vector2 functionality, but only what was needed so far. (Sorry?)
public struct IntVector2{
    
    public int x;
    public int y;
    
    public IntVector2(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    // Set
    public void Set (int new_x, int new_y){
        this.x = new_x;
        this.y = new_y;
    }
    
    // Rotations
    public void RotateCW()
    {
        int old_x = x;
        x = y;
        y = -old_x;
    }

    public void RotateCCW()
    {
        int old_x = x;
        x = -y;
        y = old_x;
    }
    
    public override string ToString(){
        return string.Format ("({0}, {1})", x ,y);
    }
    
    //Operators
    public static IntVector2 operator + (IntVector2 a, IntVector2 b)
        {
            return new IntVector2 (
                a.x + b.x,
                a.y + b.y
            ); 
        }

        public static IntVector2 operator - (IntVector2 a) {
            return new IntVector2 (
                -a.x,
                -a.y
            );
        }

        public static IntVector2 operator - (IntVector2 a, IntVector2 b)
        {
            return a + (-b);
        }

        public static IntVector2 operator * (int d, IntVector2 a)
        {
            return new IntVector2(
                d * a.x,
                d * a.y
            );
        }

        public static IntVector2 operator * (IntVector2 a, int d)
        {
            return d * a;
        }

        public static IntVector2 operator / (IntVector2 a, int d)
        {
            return new IntVector2(
                a.x / d,
                a.y / d
            );
        }
        
    // Equality
        public static bool operator == (IntVector2 lhs, IntVector2 rhs)
        {
            return lhs.x == rhs.x && lhs.y == rhs.y;
        }

        public static bool operator != (IntVector2 lhs, IntVector2 rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals (object other)
        {
            if (!(other is IntVector2))
            {
                return false;
            }
            return this == (IntVector2)other;
        }
        
        public bool Equals (IntVector2 other)
        {
            return this == other;
        }
        
        public override int GetHashCode ()
        {
            return (x.GetHashCode() << 6) ^ y.GetHashCode();
        }
        
        // Default values
        public static IntVector2 down
        {
            get { return new IntVector2 (0,-1); }
        }

        public static IntVector2 up
        {
            get { return new IntVector2 (0,+1); }
        }

        public static IntVector2 left
        {
            get { return new IntVector2 (-1,0); }
        }

        public static IntVector2 right 
        {
            get { return new IntVector2 (+1,0); }
        }

        public static IntVector2 one
        {
            get { return new IntVector2 (+1,+1); }
        }

        public static IntVector2 zero
        {
            get { return new IntVector2 (0,0); }
        }
}
