﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileData : ScriptableObject {
    public char Encoding;
    public Sprite TileSprite;
    public bool Buildable;
}
