﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : ScriptableObject {

    public Sprite EnemySprite;
    public int life;
    public float movementSpeed;
}
