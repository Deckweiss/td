﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveList : ScriptableObject {

    public List<Wave> waves;
}
