﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDataList : ScriptableObject {
    public List<TileData> Tiles;
}
