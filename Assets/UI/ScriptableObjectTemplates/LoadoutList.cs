﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadoutList : ScriptableObject {

    public List<Loadout> Loadouts;

}
