﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISpriteData : ScriptableObject {
    public string Encoding;
    public Sprite UISprite;
}
