﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loadout : ScriptableObject {

    public string Name;
    public Sprite BackgroundImage;
    public List<Tower> Towers;

}
