﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISpriteList : ScriptableObject {
    public List<UISpriteData> UISprites;
}
