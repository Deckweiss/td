﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeName : MonoBehaviour {

    [SerializeField]
    LoadoutManager manager;
    
    void OnEnable(){
        SetupNameField();
    }
    
    public void SetupNameField(){
        GetComponent<InputField>().text = LoadoutControl.control.GetSelectedLoadout().loadoutName;
    }
    
    public void SubmitName(string arg0){
        LoadoutControl.control.GetSelectedLoadout().loadoutName = arg0;
        manager.updateMenuLoadouts();
    }
}
