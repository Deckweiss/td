﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyLoadout : MonoBehaviour {
    
    [SerializeField]
    LoadoutManager manager;

    public void copy(){
        manager.CreateCopyOf(LoadoutControl.control.GetSelectedLoadout());
    }
}
