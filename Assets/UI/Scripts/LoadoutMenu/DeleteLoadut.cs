﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteLoadut : MonoBehaviour {
    
    [SerializeField]
    private LoadoutManager loadoutManager;
    
    public void delete(){
        loadoutManager.DeleteLoadoutByKey(LoadoutControl.control.GetSelectedLoadout().guid);
    }
}
