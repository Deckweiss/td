﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownControlLBG : MonoBehaviour {
    
    [SerializeField]
    private Dropdown dropdown;
    
    [SerializeField]
    private UISpriteList dataList;
    
    private LoadoutControl loadoutControl;
    private List<string> options;
    
    void Awake(){
        this.loadoutControl = LoadoutControl.control;
        options = new List<string>();
        setupList();
    }
    
    void OnEnable() {
        setupSelection();
    }
    
    private void setupList(){
        dropdown.ClearOptions();
        foreach (UISpriteData uisd in dataList.UISprites){
            options.Add(uisd.Encoding);
        }
        dropdown.AddOptions(options);
    }
    
    private void setupSelection(){
        dropdown.value = options.IndexOf(loadoutControl.GetSelectedLoadout().backgroundImageName);
    }
    
    public void changeBG(){
        loadoutControl.GetSelectedLoadout().backgroundImageName = dropdown.captionText.text;
    }
}
