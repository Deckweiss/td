﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LoadoutManager : MonoBehaviour {

    [SerializeField]
    private GameObject content;
    [SerializeField]
    private GameObject loadoutPrefab;
    
    private UISpriteManager uiSpriteManager;
    private AspectRatioScaler arScaler;
    private Dictionary<System.Guid, SerializableLoadout> serializableLoadouts;
    
    void Start(){
        arScaler = content.GetComponent<AspectRatioScaler>();
        uiSpriteManager = GetComponent<UISpriteManager>();
        serializableLoadouts = new Dictionary<System.Guid, SerializableLoadout>();
        loadLoadouts();
    }
    
    private void loadLoadouts(){
        clearMenuElements();
        if(File.Exists(Application.persistentDataPath + "/uidata")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/uidata", FileMode.Open);
            this.serializableLoadouts = (Dictionary<System.Guid, SerializableLoadout>)bf.Deserialize(file);
            file.Close();
        }
        
        foreach( KeyValuePair<System.Guid, SerializableLoadout> item in serializableLoadouts){
            GameObject newLoadout = Instantiate(loadoutPrefab);
            newLoadout.name = "Loadout Frame";
            newLoadout.GetComponent<Image>().sprite =
                uiSpriteManager.getSprite(item.Value.backgroundImageName);
            AddLoadout(newLoadout, item.Value);
        }
        arScaler.updateAspectRatio();
    }
    
    public void SaveLoadouts(){
        //print("saving");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create (Application.persistentDataPath + "/uidata");
        bf.Serialize(file, this.serializableLoadouts);
        file.Close();
    }
    
    private void AddLoadout(GameObject newLoadout, SerializableLoadout newSL){
        newLoadout.GetComponent<LoadoutMenuElement>().SetName(newSL.loadoutName);
        newLoadout.GetComponent<LoadoutMenuElement>().SetSerializableLoadout(newSL);
            
        newLoadout.transform.SetParent(content.transform, false);
        this.gameObject.transform.parent.parent.SetParent(null, false);
        this.gameObject.transform.parent.parent.SetParent(content.transform, false);
    }
    
    private void clearMenuElements(){
        content.transform.DetachChildren();
        for(int i = 0; i< content.transform.childCount-1; i++){
            Destroy(content.transform.GetChild(i).gameObject);
        }
        this.gameObject.transform.parent.parent.SetParent(content.transform, false);
    }
    
    public void updateMenuLoadouts(){
        for(int i = 0; i< content.transform.childCount-1; i++){
            LoadoutMenuElement lME =
                content.transform.GetChild(i).gameObject.GetComponent<LoadoutMenuElement>();
            lME.UpdateD(uiSpriteManager.getSprite(lME.GetSerializableLoadout().backgroundImageName));
        }
    }
    
    private SerializableLoadout getLoadoutByKey(System.Guid key){
        SerializableLoadout output;
        if (!serializableLoadouts.TryGetValue(key, out output)) {
            // the key isn't in the dictionary.
            return null;
        }
        // output is now equal to the value
        return output;
    }

    public void CreateLoadout(){
        GameObject newLoadout = Instantiate(loadoutPrefab);
        
        newLoadout.name = "Loadout Frame";
        string loadoutName = "New Loadout";
        string bgIName = "Forest";
        System.Guid guid = System.Guid.NewGuid();
        
        SerializableLoadout newSL = new SerializableLoadout(guid, loadoutName, bgIName);
        
        this.serializableLoadouts.Add(guid, newSL);
        this.SaveLoadouts();
        
        AddLoadout(newLoadout, newSL);
        arScaler.updateAspectRatio();
    }
    
    public void CreateCopyOf(SerializableLoadout loadoutToCopy){
        GameObject newLoadout = Instantiate(loadoutPrefab);
        
        newLoadout.name = "Loadout Frame";
        string loadoutName = "Copy of "+loadoutToCopy.loadoutName;
        string bgIName = loadoutToCopy.backgroundImageName;
        System.Guid guid = System.Guid.NewGuid();
        
        SerializableLoadout newSL = new SerializableLoadout(guid, loadoutName, bgIName);
        
        this.serializableLoadouts.Add(guid, newSL);
        this.SaveLoadouts();
        
        AddLoadout(newLoadout, newSL);
        arScaler.updateAspectRatio();
    }
    
    public void SelectLoadoutByKey(System.Guid key){
        LoadoutControl.control.SetSelectedLoadout(getLoadoutByKey(key));
    }
    
    public void DeleteLoadoutByKey(System.Guid guid){
        serializableLoadouts.Remove(guid);
        SaveLoadouts();
        loadLoadouts();
    }
}