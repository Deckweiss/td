﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLoadoutScript : MonoBehaviour {
    LoadoutManager manager;
    LoadoutMenuElement menuElement;
    
    void Start(){
        Transform content = this.gameObject.transform.parent.parent.parent;
        GameObject addButton = content.GetChild(content.childCount-1).GetChild(0).GetChild(0).gameObject;
        manager = addButton.GetComponent<LoadoutManager>();
        menuElement = this.gameObject.transform.parent.parent.gameObject.GetComponent<LoadoutMenuElement>();
    }

    public void SelectLoadout(){
        System.Guid key = menuElement.GetSerializableLoadout().guid;
        manager.SelectLoadoutByKey(key);
    }
}
