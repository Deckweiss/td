﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Catalogue of Sprites by their String label, declared via scriptableObjects in data.
public class UISpriteManager : MonoBehaviour {

        [SerializeField]
        private UISpriteList dataList;
        private Dictionary<string, Sprite> uiSprites;
        
        
        void Awake() {
            uiSprites = new Dictionary<string, Sprite>();
            LoadSprites();
        }
        
        void LoadSprites(){
            foreach (UISpriteData uisd in dataList.UISprites){
                uiSprites.Add(uisd.Encoding, uisd.UISprite);
            }
        }
        
        public Sprite getSprite(string s){
            Sprite output;
            if (!uiSprites.TryGetValue(s, out output)) {
                // the key isn't in the dictionary.
                return null;
            }
            // output is now equal to the value
            return output;
        }

        
        
}
