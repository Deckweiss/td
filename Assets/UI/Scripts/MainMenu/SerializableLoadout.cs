﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Holder of loadout data for serialization.
[System.Serializable]
public class SerializableLoadout{

    public System.Guid guid;
    public string loadoutName;
    public string backgroundImageName;
    
    public SerializableLoadout(System.Guid guid, string name, string bgImageName){
        this.guid = guid;
        this.loadoutName = name;
        this.backgroundImageName = bgImageName;
    }
}
