﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditLoadout : MonoBehaviour {

    public void editLoadout(){
        Transform loadoutFrame = this.gameObject.transform.parent.parent.parent;
        loadoutFrame.GetChild(0).GetChild(0).GetComponent<SelectLoadoutScript>().SelectLoadout();
        MenuNavigation.navigation.editLoadout();
    }
}
