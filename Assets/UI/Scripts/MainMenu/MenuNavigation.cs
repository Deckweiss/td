﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Switches between menu panels. 
//Neccessary, cause you cannot attatch panels as reference in prefabs, that are outside of the prefab.
public class MenuNavigation : MonoBehaviour {

    public static MenuNavigation navigation;
    
    [SerializeField]
    private GameObject mainMenuPanel;
    [SerializeField]
    private GameObject loadoutEditPanel;
    [SerializeField]
    private GameObject collectionPanel;
    [SerializeField]
    private GameObject mapsPanel;
    [SerializeField]
    private GameObject settingsPanel;

    void Start(){
        navigation = this;
    }
    
    public void editLoadout(){
        mainMenuPanel.SetActive(false);
        loadoutEditPanel.SetActive(true);
    }
    
    public void collection(){
        mainMenuPanel.SetActive(false);
        collectionPanel.SetActive(true);
    }
    
    public void maps(){
        mainMenuPanel.SetActive(false);
        mapsPanel.SetActive(true);
    }
    
    public void setting(){
        mainMenuPanel.SetActive(false);
        settingsPanel.SetActive(true);
    }
    
    public void backToMain(){
        loadoutEditPanel.SetActive(false);
        collectionPanel.SetActive(false);
        mapsPanel.SetActive(false);
        settingsPanel.SetActive(false);
        
        mainMenuPanel.SetActive(true);
    }
}
