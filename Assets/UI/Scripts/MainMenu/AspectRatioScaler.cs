﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Fixes some weird behavious with the aspectRatioFitter and background immages of buttons in a hacky way.
public class AspectRatioScaler : MonoBehaviour {
    
    private AspectRatioFitter aspectRatioFitter;
    private GameObject contentPanel;
    private float targetAspectRatio;
    
    void Awake(){
        contentPanel = this.gameObject;
        aspectRatioFitter = contentPanel.GetComponent<AspectRatioFitter>();
        targetAspectRatio = aspectRatioFitter.aspectRatio;
    }
    
    private int getNumberOfElements(){
        return contentPanel.transform.childCount;
    }
    
    //Aspect ratio for n elements is the desired ratio divided by the number of elements.
    public void updateAspectRatio(){
        int n = getNumberOfElements();
        aspectRatioFitter.aspectRatio = targetAspectRatio/n;
    }
}
