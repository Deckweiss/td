﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//The GUI representation for a loadout
public class LoadoutMenuElement : MonoBehaviour {
    
    [SerializeField]
    private UnityEngine.UI.Text loadoutName;
    [SerializeField]
    private Image loadoutImage;
    
    private SerializableLoadout loadout;

    public void UpdateD(Sprite loadoutImage){
        SetName(loadout.loadoutName);
        SetSprite(loadoutImage);
    }
    
    public void SelectThisLoadout(){
        //TODO
    }
    
    public void SetName(string loadoutName){
        this.loadoutName.text = loadoutName;
    }
    
    public void SetSprite(Sprite loadoutImage){
        this.loadoutImage.sprite = loadoutImage;
    }
    
    public void SetSerializableLoadout(SerializableLoadout loadout){
        this.loadout = loadout;
    }
    
    public SerializableLoadout GetSerializableLoadout(){
        return this.loadout;
    }
}
