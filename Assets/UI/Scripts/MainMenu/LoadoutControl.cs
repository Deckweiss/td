﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Singleton class for holding the selected loadout throuout scene switches and edits.
public class LoadoutControl : MonoBehaviour {

    public static LoadoutControl control;
    
    private SerializableLoadout selectedLoadout;

    void Awake(){
        if(control == null){
            DontDestroyOnLoad(gameObject);
            control = this;
        }else if(control != this){
            Destroy(gameObject);
        }
    }
    
    //For debugging right now.
    void OnGUI(){
        string display = "Null";
        if(selectedLoadout == null){
            display = "No loadout Selected";
        }else{
            display = "Loadout: " + selectedLoadout.loadoutName +
                      "\nbgi: " + selectedLoadout.backgroundImageName;
        }
        GUI.Label(new Rect(10, 10, 200, 50), display);
    }
    
    public void SetSelectedLoadout(SerializableLoadout loadout){
        this.selectedLoadout = loadout;
    }
    
    public SerializableLoadout GetSelectedLoadout(){
        return this.selectedLoadout;
    }
    
    public void GetLoadoutAsTowerList(){
        
    }
}
