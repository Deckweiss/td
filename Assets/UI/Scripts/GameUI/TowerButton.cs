﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerButton : MonoBehaviour {

    [SerializeField]
    private int index;
    private Sprite towerSprite;
    
    public GameObject towerGameObjectprefab;
    private GameObject concreteGameObject;
    public GameObject towerShadow;
    private GenericTower tower;
    
    
    void Start(){
        if(index > 0){
            this.disable();
        }else{
            // CREATE TOWER
            concreteGameObject = Instantiate(towerGameObjectprefab);
            concreteGameObject.name = "ConcreteTower";
            concreteGameObject.transform.parent = gameObject.transform;
            tower = concreteGameObject.AddComponent<GenericTower>();
            tower.enabled = false;
            Dictionary<TowerVarsEnum, object> towervars = new Dictionary<TowerVarsEnum, object>();
            towervars.Add(TowerVarsEnum.ViewRadius, (float)8);
            towervars.Add(TowerVarsEnum.AttackSpeed, (float)5);
            towervars.Add(TowerVarsEnum.TargetingScriptType, TargetingScriptsEnum.OnPoint);
            towervars.Add(TowerVarsEnum.TargetingMode, TargetingModesEnum.FirstinPath);
            towervars.Add(TowerVarsEnum.StayonTarget, false);
            towervars.Add(TowerVarsEnum.PathingScriptType, ProjectilePathingScriptsEnum.ToPoint);
            towervars.Add(TowerVarsEnum.ProjectileSpeed, (float)100);
            towervars.Add(TowerVarsEnum.HomingSensitivity, (float)0.5);
            towervars.Add(TowerVarsEnum.AutoDestroyAfterSecs,(float) -1);
            towervars.Add(TowerVarsEnum.DamageScriptType, DamageScriptsEnum.SingleTarget);
            towervars.Add(TowerVarsEnum.Damage, 20);
            towervars.Add(TowerVarsEnum.DamageType, 1);
            Sprite sprite = Resources.Load<Sprite>("Sprites/Projectiles/projectile");
            if(sprite == null)
            {
                Debug.Log("Sprite not loaded");
            }
            towervars.Add(TowerVarsEnum.ProjectileSprite, sprite);
            tower.setValues(towervars);
            // END CREATE TOWER
            
            towerShadow = Instantiate(towerShadow);
            towerShadow.transform.position = new Vector3(-100, -100, -100);
            towerShadow.name = "TowerShadow";
            towerShadow.GetComponent<SpriteRenderer>().sprite = concreteGameObject.GetComponent<SpriteRenderer>().sprite;
            
        }
    }
    
    //Deactivates and hides the button from the button pannel
    private void disable(){
        this.gameObject.SetActive(false);
    }
    
    //Shows and enables button in the panel
    private void enable(){
         this.gameObject.SetActive(true);
    }
    
    //Returns the tower GameObject that the button holds
    public GameObject getTower(){
        GameObject newTower=Instantiate(concreteGameObject, new Vector3(-100, -100, -100), Quaternion.identity);
        newTower.GetComponent<GenericTower>().setValues(concreteGameObject.GetComponent<GenericTower>().getValues());
        return newTower;
    }
    
    //Returns the shadow object of the tower for dragging
    public GameObject getShadow(){
        return towerShadow;
    }


}
