﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum MouseState {Hovering, Dragging, DraggingUI, Down, Up};

public class InputHandler : MonoBehaviour {
    
    private MouseState mouseState = MouseState.Hovering;
    private Vector3 startingPosition = new Vector3(0, 0, 0);
    private Vector3 mouseDragVector = new Vector3(0, 0, 0);
    private Vector3 cameraCenterPos = new Vector3(0, 0, 0);
    private float mouseDistance = 0.0f;
    private bool mouseDragThreshholdPassed = false;
    private Camera mainCamera;
    private LevelManager levelManager;
    
    private float minX = 0;
    private float minY = 0;
    private float maxX = 0;
    private float maxY = 0;

    //private bool dragging = false;
    private bool draggingUI = false;
    private bool overUI = false;

    private TowerButton selectedTowerButton;
    private GameObject towerToPlace;
    private GameObject towerShadow;
    
    private List<RaycastResult> hitObjects = new List<RaycastResult>();
    
    public void controlledUpdate(){
        mouseCheck();
    }
    
    public void initializeFields(){
        mainCamera = Camera.main;
        cameraCenterPos = mainCamera.transform.position;
        levelManager = GameManager.manager.levelManager;
        
        minX = levelManager.bound.min.x;
        minY = levelManager.bound.min.y;
        maxX = levelManager.bound.max.x;
        maxY = levelManager.bound.max.y;
    }
    
    //Returns the Unity.UI object if any. The number 2 represents the depth layer -
    //Some Buttons for now have a panel and an image on top of them, so the button is the 
    //3rd object in the hitObjects List.
    private TowerButton GetTowerButtonUnderMouse(){
        TowerButton towerButton = null;
        var pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;
        EventSystem.current.RaycastAll(pointer, hitObjects);
        
        if(hitObjects.Count > 2){
        
            towerButton = hitObjects[2].gameObject.GetComponent<TowerButton>();
        }
        return towerButton;
    }
    
    //State machine for mouse states.
    //Could make it more readable, by encapsulating everything in its own function. (TODO)
    private void mouseCheck(){
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit=Physics2D.Raycast(ray.origin, ray.direction, 1.0f);
        
        if(EventSystem.current.IsPointerOverGameObject()){ 
            overUI = true;
        }else{
            overUI = false;
        }
        
        
        switch (this.mouseState){
        case MouseState.Hovering:
        
            mouseHovering(hit, overUI);
            if(Input.GetMouseButtonDown(0)){
                selectedTowerButton = GetTowerButtonUnderMouse();
                if(selectedTowerButton != null){
                    
                    towerToPlace = selectedTowerButton.getTower();
                    towerShadow = selectedTowerButton.getShadow();
                    //print(selectedTowerButton.gameObject.transform.parent.name);
                    draggingUI = true;
                }else{
                    draggingUI = false;
                }
                this.mouseState = MouseState.Down;
            }
            
            break;
        case MouseState.Down:
        
            startingPosition = Input.mousePosition;
            if(draggingUI){
                this.mouseState = MouseState.DraggingUI;
            }else{
                this.mouseState = MouseState.Dragging;
            }
            
            break;
        case MouseState.DraggingUI:
        
            hoverTower(hit, overUI);
            
            if(Input.GetMouseButtonUp(0)){
                this.mouseState = MouseState.Up;
            }
            
            break;
        case MouseState.Dragging:
        
            mouseDragVector = mainCamera.ScreenToWorldPoint(startingPosition) -    
                              mainCamera.ScreenToWorldPoint(Input.mousePosition);
            mouseDistance = mouseDragVector.magnitude;
            
            if(mouseDistance > 0.35f){
                mouseDragThreshholdPassed = true;
            }
            if(mouseDragThreshholdPassed){
                mouseDrag(mouseDragVector);
            }
            
            
            if(Input.GetMouseButtonUp(0)){
                this.mouseState = MouseState.Up;
            }
            
            break;
        case MouseState.Up:
            if(draggingUI && !overUI){
                dropTower(hit);
            }else if(draggingUI){
                Destroy(towerToPlace);
                if(towerShadow != null){
                    towerShadow.transform.position = new Vector3(-100, -100, -100);
                }
            }else{
                mouseDragThreshholdPassed = false;
                cameraCenterPos = mainCamera.transform.position;
            }
            
            draggingUI = false;
            this.mouseState = MouseState.Hovering;
            
            break;
        }
    }
    
    //Places tower on the tile below the Raycast hit, if possible.
    private void dropTower(RaycastHit2D hit){
        //print("drop tower");
        
        if(hit.collider != null && towerToPlace != null){
            levelManager.placeTowerClick(hit, towerToPlace);
        }else if(towerToPlace != null){
            Destroy(towerToPlace);
        }
        
        if(towerShadow != null){
            towerShadow.transform.position = new Vector3(-100, -100, -100);
        }
    }
    
    // Shows the shadow of the tower while dragging it from the button.
    private void hoverTower(RaycastHit2D hit, bool overUI){
        levelManager.mouseOverOutline(hit, towerShadow, overUI);
        //print("hovering tower");
    }
    
    private void mouseHovering(RaycastHit2D hit, bool overUI){
        levelManager.mouseOverOutline(hit, overUI);
    }
    
    //Moves the level by dragging it. Restricted by the bounds, that are calculated during 
    //Level generation in the levelManager and fetched in this.initializeFields()
    private void mouseDrag(Vector3 mouseDragVector){
        
        float x = cameraCenterPos.x + mouseDragVector.x;
        float y = cameraCenterPos.y + mouseDragVector.y;
        float height = mainCamera.orthographicSize*2;
        float width = height * mainCamera.aspect;
        
        if(x - width/2 < minX){
            x = minX + width/2;
        }
        if(x + width/2 > maxX){
            x = maxX - width/2;
        }
        
        if(y - height/2 < minY){
            y = minY + height/2;
        }
        if(y + height/2 > maxY){
            y = maxY - height/2;
        }
        
        mainCamera.transform.position = new Vector3(x, y, mainCamera.transform.position.z);
    }
}
