﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionGeneratorSetting : ScriptableObject {
    public string factionName;

    //Base
    public DamageType damageType;
    //public HitType hitType;
    //public Effect effect;
    public int minDmg;
    public int maxDmg;
    
    //Weapon   
    public SecondaryDamageType secondaryDamageType;
    public double attackSpeed;
    public double range;
    public int secondaryMinDmg;
    public int secondaryMaxDmg;
    
    //Accessory
    public int minCritDmg;
    public int maxCritDmg;
    public double critChance;
    //public Effect secondaryEffect;
}
