﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorSettings : ScriptableObject {
    public List<FactionGeneratorSetting> FactionSettings;
}
