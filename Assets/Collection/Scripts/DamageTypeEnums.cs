﻿public enum DamageType{
    Physical,
    Magical
}

public enum SecondaryDamageType{
    None,
    Fire,
    Ice,
    Electic,
    Poison,
    Piercing
}
